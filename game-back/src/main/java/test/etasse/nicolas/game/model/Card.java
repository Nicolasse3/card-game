package test.etasse.nicolas.game.model;

import java.util.Objects;
import java.util.Random;

public class Card {

    private static final Random RNG = new Random();


    private final Suit suit;
    private final Pip pip;

    public Card() {
        int suitIndex = RNG.nextInt(Suit.values().length);
        this.suit = Suit.values()[suitIndex];
        int pipIndex = RNG.nextInt(Pip.values().length);
        this.pip = Pip.values()[pipIndex];
    }

    @Override
    public String toString() {
        return pip + " OF " + suit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return suit == card.suit && pip == card.pip;
    }

    @Override
    public int hashCode() {
        return Objects.hash(suit, pip);
    }

    public Suit getSuit() {
        return suit;
    }

    public Pip getPip() {
        return pip;
    }
}
