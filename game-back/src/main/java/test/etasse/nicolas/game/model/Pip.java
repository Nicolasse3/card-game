package test.etasse.nicolas.game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Pip {

    ACE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING;

    public static List<Pip> shuffleList() {
        List<Pip> pips = new ArrayList<>(List.of(Pip.values()));
        Collections.shuffle(pips);
        return pips;
    }
}
