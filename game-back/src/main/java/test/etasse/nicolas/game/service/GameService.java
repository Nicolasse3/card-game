package test.etasse.nicolas.game.service;

import test.etasse.nicolas.game.model.Card;
import test.etasse.nicolas.game.model.Pip;
import test.etasse.nicolas.game.model.Suit;

import java.util.List;

public interface GameService {


    List<Suit> shuffleSuits();

    List<Pip> shufflePips();

    List<Card> drawHand();

    List<Card> sortHand(List<Suit> s, List<Pip> p, List<Card> ul);
}
