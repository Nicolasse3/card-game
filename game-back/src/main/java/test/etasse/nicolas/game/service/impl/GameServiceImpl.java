package test.etasse.nicolas.game.service.impl;

import org.springframework.stereotype.Service;
import test.etasse.nicolas.game.model.Card;
import test.etasse.nicolas.game.model.Pip;
import test.etasse.nicolas.game.model.Suit;
import test.etasse.nicolas.game.service.GameService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class GameServiceImpl implements GameService {

    @Override
    public List<Suit> shuffleSuits() {
        return Suit.shuffleList();
    }

    @Override
    public List<Pip> shufflePips() {
        return Pip.shuffleList();
    }

    /**
     * Draw a hand of 10 unsorted cards.
     * @return A list of ten unique cards
     */
    @Override
    public List<Card> drawHand() {

        List<Card> cards = new ArrayList<>();
        Set<Card> cardSet = new HashSet<>();
        while(cards.size() < 10) {
            Card card = new Card();
            // Pour ne pas ajouter de doublons, on ajoute d'abord la nouvelle carte à un HashSet qui renvoie false si la carte est déjà présente.
            if (cardSet.add(card)) {
                cards.add(card);
            }
        }
        return cards;
    }

    /**
     * Sort a list of cards in a given order.
     * @return The list sorted according to the given order
     */
    @Override
    public List<Card> sortHand(List<Suit> s, List<Pip> p, List<Card> ul) {
        if (ul == null) return null;
        Card[] cards = ul.toArray(new Card[0]);
        for(int i = 1; i < cards.length; ++i)
        {
            Card card = cards[i];
            int j = i;
            while(j > 0 && compare(s, p, card, cards[j-1]) < 0)
            {
                cards[j] = cards[j-1];
                --j;
            }
            cards[j] = card;
        }
        return new ArrayList<>(List.of(cards));
    }

    /**
     * Compares two cards according to an order randomly generated before.
     * @param c1 The first card to compare
     * @param c2 The second card to compare
     * @return -1 if the first card comes before the second, 1 if it comes after, 0 if they're the same
     */
    public int compare(List<Suit> s, List<Pip> p, Card c1, Card c2) {
        int suit1Value = s.indexOf(c1.getSuit());
        int pip1Value = p.indexOf(c1.getPip());
        int suit2Value = s.indexOf(c2.getSuit());
        int pip2Value = p.indexOf(c2.getPip());

        if (suit1Value > suit2Value) {
            return 1;
        } else if (suit1Value < suit2Value) {
            return -1;
        } else {
            return Integer.compare(pip1Value, pip2Value);
        }
    }
}
