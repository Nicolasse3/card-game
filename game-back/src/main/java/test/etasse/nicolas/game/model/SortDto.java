package test.etasse.nicolas.game.model;

import java.util.List;

public class SortDto {

    private List<String> suits;
    private List<String> pips;
    private List<Card> cards;

    public List<String> getSuits() {
        return suits;
    }
    public List<String> getPips() {
        return pips;
    }
    public List<Card> getCards() {
        return cards;
    }
}
