package test.etasse.nicolas.game.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.etasse.nicolas.game.model.Card;
import test.etasse.nicolas.game.model.Pip;
import test.etasse.nicolas.game.model.SortDto;
import test.etasse.nicolas.game.model.Suit;
import test.etasse.nicolas.game.service.GameService;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600, allowedHeaders = "*")
@RestController
@RequestMapping("game/")
public class GameRestController {

    GameService gameService;

    @GetMapping("shuffle/suits")
    public List<Suit> shuffleSuits() {
        return gameService.shuffleSuits();
    }

    @GetMapping("shuffle/pips")
    public List<Pip> shufflePips() {
        return gameService.shufflePips();
    }

    @GetMapping("draw")
    public List<Card> drawHand() {
        return gameService.drawHand();
    }

    @PostMapping("sort")
    public List<Card> sortHand(@RequestBody SortDto b) {
        List<Suit> suits = b.getSuits().stream().map(Suit::valueOf).toList();
        List<Pip> pips = b.getPips().stream().map(Pip::valueOf).toList();
        return gameService.sortHand(suits, pips, b.getCards());
    }

    @Autowired
    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }
}
