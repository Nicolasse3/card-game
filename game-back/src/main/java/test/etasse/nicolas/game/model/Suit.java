package test.etasse.nicolas.game.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum Suit {

    DIAMONDS, CLUBS, HEARTS, SPADES;

    public static List<Suit> shuffleList() {
        List<Suit> suits = new ArrayList<>(List.of(Suit.values()));
        Collections.shuffle(suits);
        return suits;
    }
}
