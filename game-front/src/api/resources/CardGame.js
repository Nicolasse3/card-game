import { APISettings } from "../config";

export default {

    shuffleSuits(){
        return this.get('shuffle/suits');
    },

    shufflePips(){
        return this.get('shuffle/pips');
    },

    drawHand(){
        return this.get('draw');
    },

    get(endPoint){
        return fetch( APISettings.baseURL + endPoint, {
            method: 'GET',
            headers: APISettings.headers
        }).then(function (response) {
            if (response.status !== 200) {
                throw response.status;
            } else {
                return response.json();
            }
        })
    },

    sortHand(data){
        return fetch(APISettings.baseURL + 'sort', {
            method: 'POST',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(data)
        }).then(function (response) {
            if (response.status !== 200) {
                throw response.status;
            } else {
                return response.json();
            }
        })
    }
}